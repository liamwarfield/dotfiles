# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# Sets up screens
xrandr --output DP-3 --left-of eDP-1
xrandr --output DP-1 --rotate left
xrandr --output eDP-1 --mode 1920x1080
# User specific aliases and functions
export TILLER_NAMESPACE="joules-helm"    # Add this line to your .bashrc
alias ls='ls --color'
alias alpr='isengard lpr -P bb136-printer -o coallate=true'
alias alprd='isengard lpr -P bb136-printer -o coallate=true -o Duplex=DuplexNoTumble'
alias flake8="clear; flake8"
alias c="clear"
alias gitc="git checkout"
alias gita="git add"
alias gits="git status"
alias grep="grep --color -n"
alias imagine="ssh liamwarfield@imagine.mines.edu"
alias isengard="ssh liamwarfield@isengard.mines.edu"
alias ls="ls --color"
alias shh="sl"
alias soviet="amixer set Master on 50% && mpv --quiet -vo caca 'https://www.youtube.com/watch?v=U06jlgpMtQs'"
alias ocg="watch -n 2 oc get all"
alias ocr="oc describe"

