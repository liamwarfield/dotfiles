# Pulls backgrounds from ~/Backgrounds and randomly display a couple

HORZONTAL1=$(ls -p ~/Backgrounds/ | \grep -v / | shuf -n 1)
HORZONTAL2=$(ls -p ~/Backgrounds/ | \grep -v / | shuf -n 1)
VERTICAL1=$(ls -p ~/Backgrounds/vertical/ | \grep -v / | shuf -n 1)

feh --bg-scale ~/Backgrounds/$HORZONTAL1 \
    ~/Backgrounds/vertical/$VERTICAL1 \
    ~/Backgrounds/$HORZONTAL2
